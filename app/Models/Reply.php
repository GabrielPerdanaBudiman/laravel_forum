<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use HasFactory;
    protected $table = "reply";
    protected $fillable = ['users_id','threads_id','comment','created_at','updated_at'];
}
