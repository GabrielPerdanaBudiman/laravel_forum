<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sub_Reply extends Model
{
    use HasFactory;
    protected $table = "sub_reply";
    protected $fillable = ['users_id','reply_id','sub_comment','created_at','updated_at'];
}
