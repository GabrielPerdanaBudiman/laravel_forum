<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function thread(){
        return view('pages.thread');
    }

    public function submit(){
        return view('pages.submit');
    }

    public function comment(){
        return view('pages.subcomment');
    }

}
