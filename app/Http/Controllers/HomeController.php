<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Thread;

class HomeController extends Controller
{
    public function home()
    {
        $thread = Thread::all();
        return view('pages.index', compact('thread'));
    }
}
