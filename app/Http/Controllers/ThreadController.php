<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Thread;
use App\Models\Tags;
use App\Models\Users;
use File;

use Illuminate\Support\Facades\Log;


class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $thread = Thread::all();
        return view('pages.index', compact('thread'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tags::all();
        return view('pages.submit', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'topic' => 'required',
            'tags_id' => 'required',
            'img' => 'required|image|mimes:jpeg,png,jpg',
            'description' => 'required',
            
        ]);

        $input = $request->all();

        if ($image = $request->file('img')) {
                $destinationPath = 'img';
                $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $input['img'] = "$profileImage";
            }

        Thread::create($input);
            
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $thread = Thread::findorfail($id);
        return view('pages.thread', compact('thread'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags = Tags::all();
        $thread = Thread::findorfail($id);
        return view('pages.edit', compact('thread','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'topic' => 'required',
            'tags_id' => 'required',
            'img' => 'required|image|mimes:jpeg,png,jpg',
            'description' => 'required',
            
        ]);

        $thread = Thread::findorfail($id);

        if ($request->has('img')){
            $path = "img/";
            File::delete($path . $thread->img);
            $profileImage = date('YmdHis') . "." . $request->img->extension();
            $request->img->move(public_path('img'), $profileImage);
            $input = [
                'topic' => $request->topic,
                'tags_id' => $request->tags_id,
                'img' => $profileImage,
                'description' => $request->description
            ];
        } else {
            $input = [
                'topic' => $request->topic,
                'tags_id' => $request->tags_id,
                'description' => $request->description
            ]; 
        }

        $thread->update($input);

        return redirect('/thread');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thread = Thread::findorfail($id);

        $path = "img/";
        File::delete($path . $thread->img);

        $thread->delete();

        return redirect('/thread');
    }
}
