<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ForumController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ThreadController;
use App\Http\Controllers\UsersController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//----------------------------Index-------------------------------------------
Route::get('/', [HomeController::class, 'home']);

//----------------------------Forum-------------------------------------------
// Route::get('/thread', [ForumController::class, 'thread']);
// Route::get('/submit', [ForumController::class, 'submit']);
// Route::get('/comment', [ForumController::class, 'comment']);

//----------------------------Authentication----------------------------------
Route::get('/register', [AuthController::class, 'register']);
Route::post('/register', [AuthController::class, 'store']);
Route::get('/login', [AuthController::class, 'login']);
//Route::get('/success', 'App\Http\Controllers\Auth\AuthController@success');

//----------------------------Profile----------------------------------------
Route::get('/users_profile', [UsersController::class, 'users_profile']);

//----------------------------CRUD Thread----------------------------------------
Route::resource('thread', ThreadController::class);

//----------------------------CRUD Profile----------------------------------------
Route::resource('users', UsersController::class);