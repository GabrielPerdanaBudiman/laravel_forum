<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm">

    <img id="logo" class="d-flex navbar-nav mr-auto" src="{{asset('img/logo.png')}}" width="200px" />

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <form class="form-inline my-4 my-lg-0 mx-lg-5 w-50">
            <input class="form-control mr-sm-2 w-50" type="search" placeholder="Cari" aria-label="Cari">
            <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
        </form>
        
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
            <a class="nav-link" href="/login">Login<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
            <a class="nav-link" href="/register">Sign Up</a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Opsi
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Tutorial HTML</a>
                <a class="dropdown-item" href="#">Tutorial CSS</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Tutorial Bootstrap</a>
            </div>
            </li>
        </ul>

    </div>
</nav>