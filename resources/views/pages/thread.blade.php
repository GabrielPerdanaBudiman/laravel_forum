@extends('layout.main')

@section('isi')

  <div class="container mt-5 pb-5" >     
    
    <div class="card m-0 rounded-0 border-0 shadow-sm">
        <div class="border-bottom">
            <div class="card-header bg-white border-0">
                <div class="media flex-wrap w-100 align-items-center"> <img src="{{asset('img/1.png')}}" class="d-block ui-w-40 rounded-circle" height="44px">
                    <div class="media-body ml-3"> <a href="" >Tom Harry</a>
                        <div class="text-muted small">13 days ago</div>
                    </div>
                    <div class="text-muted small ml-3">
                        <div>Member since <strong>01/1/2019</strong></div>
                        <div><strong>134</strong> posts</div>
                    </div>
                </div>
            </div>
            <div class="card-body p-3 border-0">
                <img class="card-img mb-2" src="{{asset('img/'.$thread->img)}}" alt="card-image-cap">
                <p class="card-text">{{Str::limit($thread->description, 15)}}</p>
            </div>
            <div class="card-footer d-flex flex-wrap bg-white border-0 justify-content-start align-items-center px-0 pt-0 pb-3">
            </div>
        </div>
    </div> 
    
    
    <form>
        <div class="card mt-5">
          <div class="card-body">
            <div class="form-group">
              <label for="exampleFormControlTextarea1">Example textarea</label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
          </div>
          <div class="card-footer bg-white border-0">
            <button class="btn btn-dark float-right" type="submit">Post</button>
          </div>
        </div>
    </form>

    <div class="card m-0 mt-2 rounded-0 border-0 shadow-sm">
        <div class="border-bottom">
            <div class="card-header bg-white border-0">
                <div class="media flex-wrap w-100 align-items-center"> <img src="{{asset('img/1.png')}}" class="d-block ui-w-40 rounded-circle" height="44px">
                    <div class="media-body ml-3"> <a href="" >Tom Harry</a>
                        <div class="text-muted small">13 days ago</div>
                    </div>
                    <div class="text-muted small ml-3">
                        <div>Member since <strong>01/1/2019</strong></div>
                        <div><strong>134</strong> posts</div>
                    </div>
                </div>
            </div>
            <div class="card-body p-3 border-0">
                <p> For me, getting my business website made was a lot of tech wizardry things. Thankfully i get an ad on Facebook ragarding commence website. I get connected with BBB team. They made my stunning website live in just 3 days. With the increase demand of online customers. I had to take my business online. BBB Team guided me at each step and enabled me to centralise my work and have control on all aspect of my online business. </p>
            </div>
            <div class="card-footer d-flex bg-white border-0 justify-content-start align-items-center px-0 pt-0 pb-3">
                <div class="px-3"> 
                    <a href="" class="text-muted d-inline-flex align-items-center align-middle"> 
                        Comments 
                    </a>
                </div>
                <button class="btn btn-dark ml-auto mr-3" type="submit">Post</button>
            </div>
        </div>
    </div>

  </div>

@endsection
