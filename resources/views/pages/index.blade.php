@extends('layout.main')

@section('isi')

<div class="container-fluid mb-3 pt-3 h-100 bg-light mx-0 h-100" >      
    <div class="row ">
      <nav class="col-md-2 d-none d-md-block navbar-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link active" href="#">
                <span data-feather="home"></span>
                Dashboard <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file"></span>
                Orders
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="shopping-cart"></span>
                Products
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="users"></span>
                Customers
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="bar-chart-2"></span>
                Reports
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="layers"></span>
                Integrations
              </a>
            </li>
          </ul>
      
          <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Saved reports</span>
            <a class="d-flex align-items-center text-muted" href="#">
              <span data-feather="plus-circle"></span>
            </a>
          </h6>
          <ul class="nav flex-column mb-2">
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Current month
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Last quarter
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Social engagement
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Year-end sale
              </a>
            </li>
          </ul>
        </div>
      </nav>
      <div class="col-md-10 ml-sm-auto col-lg-10">
        <div class="row d-none d-md-block justify-content-center">
          {{-- contentnav --}}
          <nav class="navbar nav-fill w-100 navbar-light justify-content-center pt-2 navbar-expand-lg d-flex">
            <ul class="navbar-nav mr-auto justify-content-center">
              <li class="nav-item"><a class="nav-link" href="#">Website Name</a></li>
              <li class="nav-item"><a class="nav-link" href="#">Website Name</a></li>
              <li class="nav-item"><a class="nav-link" href="#">Website Name</a></li>
              <li class="nav-item"><a class="nav-link" href="#">Website Name</a></li>
            </ul>
            <a href="/thread/create" class="btn btn-dark my-2 my-sm-0 ml-auto">New Thread</a>
          </nav>
          {{-- contentnav end --}}
        </div>
      <div class="row">
        <div class="col-md-8 ml-sm-auto col-lg-8">
          @forelse ($thread as $item)
          <div class="card m-0 rounded-0 border-0 shadow-sm">
              <div class="border-bottom">
                  <div class="card-header bg-white border-0">
                      <div class="media flex-wrap w-100 align-items-center"> <img src="{{asset('img/1.png')}}" class="d-block ui-w-40 rounded-circle" height="44px">
                          <div class="media-body ml-3"> <a href="" >Tom Harry</a>
                              <div class="text-muted small">13 days ago</div>
                          </div>
                          <div class="text-muted small ml-3">
                              <div>Member since <strong>01/1/2019</strong></div>
                              <div><strong>134</strong> posts</div>
                          </div>
                      </div>
                  </div>
                  <div class="card-body p-3 border-0">
                      <img class="card-img mb-2" src="{{asset('img/'.$item->img)}}" alt="card-image-cap">
                      <h4>{{$item->topic}}</h4>
                      <p class="card-text">{{Str::limit($item->description, 100)}}</p>
                  </div>
                  <form action="{{route('thread.destroy',$item->id)}}" method="post">
                    @csrf
                    @method('delete')
                  <div class="card-footer d-flex bg-white border-0 justify-content-start align-items-center px-0 pt-0 pb-3">
                    <div class="px-3"> 
                        <a href="{{route('thread.show',$item->id)}}" class="d-inline-flex align-items-center align-middle"> 
                            Comment 
                        </a>
                    </div>
                    <div class="px-3"> 
                      <a href="{{route('thread.edit',$item->id)}}" class="d-inline-flex align-items-center align-middle"> 
                          Edit
                      </a>
                    </div>
                    
                      <input type="submit" class="btn btn-dark ml-auto mr-3" value="delete">
                    
                  </div>
                </form>
              </div>
          </div>
          @empty
              
          @endforelse              
        </div>
        <div class="col-md-4 d-none d-md-block w-100">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                  <h1 class="display-4">Ads Here</h1>
                  <p class="lead">This where the magic do, please click the linke <a class="text-dark" href="https://youtu.be/dQw4w9WgXcQ"><b>HERE</b></a></p>
                </div>
            </div>
        </div>
        </div>  
      </div>
    </div>   
</div>

@endsection