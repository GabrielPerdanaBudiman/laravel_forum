@extends('layout.main')

@section('isi')

<div class="container">      
  <form action="/thread" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card mt-5">
      <div class="card-body">
        <div class="form-group">
          <label>Title</label>
          <input type="text" name="topic" class="form-control" id="exampleFormControlInput1">
        </div>
        @error('topic')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label>Tags</label>
          <select name="tags_id" class="form-control">
            <option value="">-- Pilih Tag --</option>
            @foreach ($tags as $item)
                <option value="{{$item->id}}">{{$item->tag}}</option>
            @endforeach
          </select>
        </div>
        @error('tag')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label>Images</label>
          <input type="file" name="img" class="form-control-file" id="exampleFormControlFile1">
        </div>
        @error('img')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label>Isi</label>
          <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
        </div>
        @error('description')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="card-footer bg-white border-0">
        <button class="btn btn-dark float-right" type="submit">Post</button>
      </div>
    </div>
  </form>
</div>

@endsection
