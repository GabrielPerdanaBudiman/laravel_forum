<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    @stack('style')

    <title>Sanber Forum</title>
  </head>
  <body class="bg-light">
    {{-- navbar --}}
    <nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm">

      <img id="logo" class="d-flex navbar-nav mr-auto" src="{{asset('img/logo.png')}}" width="200px" />
  
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
  
          <form class="form-inline my-4 my-lg-0 mx-lg-5 w-50">
              <input class="form-control mr-sm-2 w-50" type="search" placeholder="Cari" aria-label="Cari">
              <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
          </form>
          
          <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
              <a class="nav-link" href="/">Return to home<span class="sr-only">(current)</span></a>
              </li>
          </ul>
  
      </div>
  </nav>
    {{-- navbar end --}}
    
    {{-- content --}}
    @yield('')
    <div class="container pb-5">
      <div class="row justify-content-center">
      
        <div class="col-md-4">
          
        <main class="card form-registration mt-5">
          <form method="POST" action="{{url('/users')}}">
            @csrf
            
            <img class="mb-4" src="{{asset('img/logo.png')}}" width="200px">
            <h1 class="h3 mb-3 fw-normal">Register here</h1>
        
            <div class="form-floating pb-2">
              <input type="name" class="form-control" id="name" placeholder="Name">
            </div>
      
            <div class="form-floating pb-2">
              <input type="username" class="form-control" id="username" placeholder="Username">
            </div>
            
            <div class="form-floating pb-2">
              <input type="email" class="form-control" id="email" placeholder="Email">
            </div>
      
            <div class="form-floating pb-2">
              <input type="password" class="form-control" id="password" placeholder="Password">
            </div>
      
            <button class="w-100 btn btn-lg btn-primary mt-3" type="submit">Register</button>
            
          </form>
          <small class="d-block text-center mt-3">Already registered ? <a href="/login">Login</a></small>
        </main>
      
        </div>
      </div>
    </div>
    {{-- content end --}}
    

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>


